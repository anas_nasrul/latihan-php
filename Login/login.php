<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
</head>
<body>
	<h2>Login</h2>
	<br/>
	<!-- cek pesan notifikasi -->
	<?php 
		if( isset($_COOKIE['id']) && isset($_COOKIE['key']) ) {
			$id=$_COOKIE['id'];
			$key=$_COOKIE['key'];

			//ambil username berdasarkan id
			$result = mysqli_query($conn, "SELECT username FROM user WHERE id=$id");
			$row = mysqli_fetch_assoc($result);

			//cek cookie dan username
			if( $key === hash('sha256', $row['username']) ){
			$_SESSION['login'] = true;	
			}

		}

		if(isset($_SESSION["login"])) {
			header("location:home.php");
			exit;
		}
	if(isset($_GET['pesan'])){
		if($_GET['pesan'] == "gagal"){
			echo "Login gagal! username dan password salah!";
		}else if($_GET['pesan'] == "logout"){
			echo "Anda telah berhasil logout";
		}else if($_GET['pesan'] == "belum_login"){
			echo "Anda harus login untuk mengakses halaman admin";
		}
	}
	?>
	<br/>
	<br/>
	<form method="post" action="cek_login.php">
		<table>
			<tr>
				<td>Username</td>
				<td>:</td>
				<td><input type="text" name="username" placeholder="Masukkan username"></td>
			</tr>
			<tr>
				<td>Password</td>
				<td>:</td>
				<td><input type="password" name="password" placeholder="Masukkan password"></td>
			</tr>
			<tr>
				<td>Remember Me</td>
				<td>:</td>
				<td><input type="checkbox" name="remember" placeholder="cek remember"></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><input type="submit" value="LOGIN"></td>
			</tr>
		</table>			
	</form>
</body>
</html>