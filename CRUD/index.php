<?php
require 'functions.php';

//pagination, konfigurasi

$jumlahDataPerHalaman = 3;
$jumlahData = count(query("SELECT * FROM mahasiswa"));
$jumlahHalaman = ceil($jumlahData / $jumlahDataPerHalaman);
$halamanAktif = ( isset ($_GET["Halaman"]) ) ? $_GET["Halaman"] : 1;
$awalData=($jumlahDataPerHalaman * $halamanAktif) - $jumlahDataPerHalaman;

$mhs = query("SELECT * FROM mahasiswa LIMIT $awalData, $jumlahDataPerHalaman");

//cari
if (isset($_POST["cari"]) ) {
	$mhs=cari($_POST["keyword"]);
}

?>

<!DOCTYPE html>
<html>
	<head>
		<title>Halaman Admin</title>
		<script src="js/jquery-3.4.1.min.js"></script>
		<script src="js/script.js"></script>
	</head>
		<body>
			
			<h1>Daftar Mahasiswa</h1>

			<a href="tambah.php">Tambah data mahasiswa</a> | <a href="cetak.php" targer="_blank">Cetak</a>

			<form action="" method="post">

				<input type="text" name="keyword" size="40" autofocus placeholder="masukkan keyword pencarian" autocomplete="off" id="keyword">
				<button type="submit" name="cari" id="tombol-cari">Cari</button>
			</form>

			<br><br>

			<!-- navigasi -->
			<?php if ($halamanAktif > 1) : ?>
			<a href="?Halaman= <?= $halamanAktif - 1; ?>">&laquo;</a>
			<?php endif; ?>


			<?php for ($i = 1; $i <= $jumlahHalaman; $i++) : ?>
				<?php if ($i == $halamanAktif) : ?>
				<a href="?Halaman=<?= $i; ?>" style="font-weight: bold; color:red;">
					<?= $i; ?></a>
				<?php else : ?>
				<?php endif; ?>
			<?php endfor; ?>

			<?php if ($halamanAktif < $jumlahHalaman) : ?>
			<a href="?Halaman= <?= $halamanAktif + 1; ?>">&raquo;</a>
			<?php endif; ?>


			<div id="container">
			<table border=1 cellpadding="10" cellspacing="0">
				<tr>
					<th>No</th>
					<th>Aksi</th>
					<th>Gambar</th>
					<th>NRP</th>
					<th>Nama</th>
					<th>Email</th>
					<th>Jurusan</th>	
				</tr>

				<?php $i = 1; ?>
				<?php foreach ( $mhs as $row) : ?>

				<tr>
					<td> <?= $i; ?> </td>
					<td>
						<a href="ubah.php?ID=<?= $row["ID"]; ?>">ubah</a> |
						<a href="hapus.php?ID=<?= $row["ID"]; ?>">hapus</a>
					</td>
					<td> <img src="img/<?= $row["GAMBAR"]; ?>" width="50"> </td>
					<td> <?= $row["NRP"]; ?> </td>
					<td> <?= $row["NAMA"]; ?> </td>
					<td> <?= $row["EMAIL"]; ?> </td>
					<td> <?= $row["JURUSAN"]; ?> </td>
				</tr>
				<?php $i++; ?>
				<?php endforeach; ?>
			</table>
			</div>
			
			
		</body>
</html>