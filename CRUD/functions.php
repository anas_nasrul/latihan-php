<?php
//koneksi mahasiswa
$conn = mysqli_connect("localhost", "root", "admin", "registrasi");

	function query($query) {
		global $conn;
		//query mahasiswa
		$result = mysqli_query($conn, $query);
		$rows=[];
		while($row=mysqli_fetch_assoc($result)) {
			$rows[]=$row;
		}
		return $rows;
	}

	function tambah($data) {
		global $conn;
		
		$NRP = htmlspecialchars($data["NRP"]);
		$NAMA = htmlspecialchars($data["NAMA"]);
		$EMAIL = htmlspecialchars($data["EMAIL"]);
		$JURUSAN = htmlspecialchars($data["JURUSAN"]);

		//upload gambar
		$GAMBAR = upload();
		if(!$GAMBAR) {
			return false;
		}

		$tmbh = "INSERT INTO mahasiswa VALUES (null, '$NRP', '$NAMA', '$EMAIL', '$JURUSAN', '$GAMBAR')";
		mysqli_query($conn, $tmbh);

		return mysqli_affected_rows($conn);
	}

	function hapus($ID) {
		global $conn;
		mysqli_query($conn, "DELETE FROM mahasiswa WHERE ID = $ID");
		return mysqli_affected_rows($conn);
	}

	function ubah($data) {
		global $conn;
		
		$ID = $data["ID"];
		$NRP = htmlspecialchars($data["NRP"]);
		$NAMA = htmlspecialchars($data["NAMA"]);
		$EMAIL = htmlspecialchars($data["EMAIL"]);
		$JURUSAN = htmlspecialchars($data["JURUSAN"]);
		$gambarLama = htmlspecialchars($data["gambarLama"]);

		//cek tidak atau ada upload, 4=tidak ada upload
			if ($_FILES['GAMBAR']['error'] === 4) {
				$GAMBAR = $gambarLama;
			} else {
				$GAMBAR = upload();
			}

		

		$ubah = "UPDATE mahasiswa SET NRP = '$NRP', NAMA = '$NAMA', EMAIL = '$EMAIL', JURUSAN = '$JURUSAN', GAMBAR = 'GAMBAR' WHERE ID = $ID ";
		mysqli_query($conn, $ubah);

		return mysqli_affected_rows($conn);
	}

	function cari($keyword) {
		
		$cari = "SELECT * FROM mahasiswa WHERE NAMA LIKE '%$keyword%' OR 
		NRP LIKE '%$keyword%' OR
		EMAIL LIKE '%$keyword%' OR
		JURUSAN LIKE '%$keyword%' ";
		return query($cari);
	}

		function upload() {
			$namaFile=$_FILES['GAMBAR']['name'];
			$ukuranFile=$_FILES['GAMBAR']['size'];
			$error=$_FILES['GAMBAR']['error'];
			$tmpName=$_FILES['GAMBAR']['tmp_name'];

			//cek tidak atau ada upload, 4=tidak ada upload
			if ($error === 4) {
				echo "pilih gambar terlebih dahulu";
				return false;
			}
			//cek apakah yang diupload gambar
			$ekstensiGambarValid = ['jpg', 'jpeg', 'png'];
			$ekstensiGambar = explode('.', $namaFile);
			$ekstensiGambar = strtolower(end($ekstensiGambar));

			if (!in_array($ekstensiGambar, $ekstensiGambarValid	)) {
				echo "anda upload bukan gambar";
				return false;
			}
			//cek size
			if($ukuranFile	> 1000000) {
				echo "ukuran gambar terlalu besar";
				return false;
			}
			//cek upload, generate gambar baru
			$namaFileBaru = uniqid();
			$namaFileBaru .= ".";
			$namaFileBaru .= $ekstensiGambar;

			move_uploaded_file(	$tmpName,'img/'.$namaFileBaru);
			return $namaFile;
		}
?>