<?php
//koneksi
require 'functions.php';

//cek submit
if (isset($_POST["submit"]) ) {
	
	//cek input
		if (tambah($_POST) > 0) {
			echo "data berhasil ditambahkan";
		} else {
			echo "data gagal ditambahkan";
		}

}


?>

<!DOCTYPE html>
<html>
	<head>
		<title>Tambah data mahasiswa</title>
	</head>
		<body>
			<h1>Tambah data mahasiswa</h1>

			<form action ="" method ="post" enctype="multipart/form-data">
				<ul>
					<li>
						<label for="NRP">NRP:</label>
						<input type="text" name="NRP" id="NRP">
					</li>
					<li>
						<label for="NAMA">NAMA:</label>
						<input type="text" name="NAMA" id="NAMA">
					</li>
					<li>
						<label for="EMAIL">EMAIL:</label>
						<input type="text" name="EMAIL" id="EMAIL">
					</li>
					<li>
						<label for="JURUSAN">JURUSAN:</label>
						<input type="text" name="JURUSAN" id="JURUSAN">
					</li>
					<li>
						<label for="GAMBAR">GAMBAR:</label>
						<input type="file" name="GAMBAR" id="GAMBAR">
					</li>
					<li>
						<button type="submit" name="submit">Tambah data</button>
					</li>
				</ul>
			</form>

		</body>
</html>