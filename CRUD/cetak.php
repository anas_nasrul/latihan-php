<?php

require_once __DIR__ . '/vendor/autoload.php';

$mpdf = new \Mpdf\Mpdf();

$html='<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="UTF-8">
	<title>Document</title>

</head>
<body>
	<h1> Daftar mahasiswa</h1>
	<table border=1 cellpadding="10" cellspacing="0">
				<tr>
					<th>No</th>
					<th>Gambar</th>
					<th>NRP</th>
					<th>Nama</th>
					<th>Email</th>
					<th>Jurusan</th>	
				</tr>';


		$i = 1;
		foreach($mhs as $row) {
		$html .= '<tr>
				<td>'. $i++ .' </td>
				<td><img src="img/'. $row["GAMBAR"] .'" width="50"></td>
				<td>'. $row["NRP"] .'</td>
				<td>'. $row["NAMA"] .'</td>
				<td>'. $row["EMAIL"] .'</td>
				<td>'. $row["JURUSAN"] .'</td>
		</tr>';
		}

$html .=' </table>
</body>
</html>';

$mpdf->WriteHTML($html);
$mpdf->Output('daftar-mahasiswa.pdf, \Mpdf\Output\Destination::INLINE');
?>

