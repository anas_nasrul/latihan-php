<?php
//koneksi
require 'functions.php';

//ambil data diurl
$ID = $_GET["ID"];

//query
$ubah = query("SELECT  * FROM mahasiswa WHERE ID = $ID")[0];


//cek submit
if (isset($_POST["submit"]) ) {
	
	//cek input
		if (ubah($_POST) > 0) {
			echo "data berhasil diubah";
		} else {
			echo "data gagal diubah";
		}

}


?>

<!DOCTYPE html>
<html>
	<head>
		<title>Ubah data mahasiswa</title>
	</head>
		<body>
			<h1>Ubah data mahasiswa</h1>

			<form action ="" method ="post" enctype="multipart/form-data">
				<input type="hidden" name="ID" value="<?= $ubah ["ID"]; ?>">
				<input type="hidden" name="gambarLama" value="<?= $ubah ["GAMBAR"]; ?>">
				<ul>
					<li>
						<label for="NRP">NRP:</label>
						<input type="text" name="NRP" id="NRP" required value="<?= $ubah ["NRP"]; ?>">
					</li>
					<li>
						<label for="NAMA">NAMA:</label>
						<input type="text" name="NAMA" id="NAMA" value="<?= $ubah ["NAMA"]; ?>">
					</li>
					<li>
						<label for="EMAIL">EMAIL:</label>
						<input type="text" name="EMAIL" id="EMAIL" value="<?= $ubah ["EMAIL"]; ?>">
					</li>
					<li>
						<label for="JURUSAN">JURUSAN:</label>
						<input type="text" name="JURUSAN" id="JURUSAN" value="<?= $ubah ["JURUSAN"]; ?>">
					</li>
					<li>
						<label for="GAMBAR">GAMBAR:</label>
						<img src="img/<?=$mhs['GAMBAR']; ?>" width="40"><br>
						<input type="file" name="GAMBAR" id="GAMBAR">
					</li>
					<li>
						<button type="submit" name="submit">Ubah data</button>
					</li>
				</ul>
			</form>

		</body>
</html>